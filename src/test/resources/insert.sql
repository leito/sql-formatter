-- -----------------------------------------------------------------------------
-- Test file for SqlFormatter
-- Comments and empty lines should be ignored.
-- -----------------------------------------------------------------------------

-- Some simple statements, all mixed up
insert into movie (id, title, release_date, enabled)
values (1, 'Cloud Atlas', '2009-12-11', true);

insert into genre (id, description)
values (1, 'Drama'), (2, 'Terror'), (3, 'Action');

insert into movie (id, title, release_date, enabled)
values (2, 'The Place Beyond The Pines', '2011-11-04', false);

insert into genre (id, description) values (4, 'Comedy');

-- Statements that use new columns, not used before.
insert into genre (id, description, enabled)
values (5, 'Sci/Fi', true),
(6, 'Adventure', true),
(7, 'Documental', false);

insert into movie (id, title, release_date, enabled, id_genre)
values (3, 'Grand Hotel Budapest', '2011-11-04', false, 5);

-- Statements with different column order
insert into movie (title, enabled, id)
values ('Pulp Fiction', true, 5), ('Inglorious Basterds', true, 6);

-- Statements with special chars
insert into movie (id, title, release_date, enabled)
values (7, 'áéíóú // ÁÉÍÓÚ // ñÑ', '2016-11-04', false);

-- Some emtpy lines





-- This should be the last rows in each table
insert into movie (id, title) values (999, 'The Last Movie');
insert into genre (id, description) values (99, 'Last Genre');
