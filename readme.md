# IMPORTANT: This project is now on GitLab
Please visit https://gitlab.com/ldeseta/SQL-Formatter

# SQL Formatter
##### A simple tool that formats SQL insert statements

## What is SQL Formatter?
SQL Formatter is Java utility that formats SQL insert statments to create a more
readable script.

SQL Formatter groups multiple SQL statements for the same table into a single
statement, and it formats all the columns to align its values.

### What statements does is format?
Currently SQL Formatter only works with insert statements. If any other statement
is present in the script it will fail.

Comments are ignored and removed from the final result.

### Example
SQL Formatter transforms a script like this...
```
#!sql
insert into movie (id, title, release_date)
values (1, 'Cloud Atlas', '2009-12-11');

insert into genre (id, description)
values (1, 'Drama'), (2, 'Terror'), (3, 'Action');

insert into movie (id, title, enabled)
values (2, 'The Place Beyond The Pines', true);
```

... into this:
```
#!sql
-- -----------------------------------------------------------------------------
-- movie
-- -----------------------------------------------------------------------------
INSERT INTO movie
(id  , title                       , release_date, enabled) VALUES
(1   , 'Cloud Atlas'               , '2009-12-11', NULL   ),
(2   , 'The Place Beyond The Pines', NULL        , true   );


-- -----------------------------------------------------------------------------
-- genre
-- -----------------------------------------------------------------------------
INSERT INTO genre
(id  , description) VALUES
(1   , 'Drama'    ),
(2   , 'Terror'   ),
(3   , 'Action'   );
```

## Usage
### Maven
```
#!xml
<dependency>
    <groupId>org.bitbucket.leito</groupId>
    <artifactId>sqlformatter</artifactId>
    <version>1.0.1</version>
</dependency>
```

### Code example
```
#!java
String sql = new String(Files.readAllBytes(Paths.get("path-to-script.sql"))); //contains one or multiple insert statements.
SqlFormatter formatter = new SqlFormatter();
String output = formatter.format(sql);
```

Also, the class SqlFormatterTest.java has several usage examples.

## Web Application
Try [Web SQL Formatter](http://sqlformatter.somospnt.com/) to live test SQL Formatter.

## License
SQL Formatter is distributed under the Mozilla Public License, version 2.0.
The LICENSE file contains more information about the licesing of this product.
You can read more about the MPL at [Mozilla Public License FAQ](https://www.mozilla.org/en-US/MPL/2.0/FAQ/).