/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package org.bitbucket.leito.sqlformatter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class App {

    /**
     * Main class for command-line execution.
     * Usage: java -jar sqlformatter.jar inputFilePath
     * @param args one mandatory argument, the path to the input file.
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {
        if (!argsAreValid(args)) {
            System.exit(-1);
        }
        String inputFile = args[0];
        String sql = fileToString(inputFile);
        SqlFormatter formatter = new SqlFormatter();
        String output = formatter.format(sql);
        System.out.println(output);
        System.exit(0);
    }

    private static String fileToString(String path) throws IOException {
        return new String(Files.readAllBytes(Paths.get(path)), "utf-8");
    }

    private static boolean argsAreValid(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage: java -jar sqlformatter.jar inputFile");
            return false;
        }
        return true;
    }
}
